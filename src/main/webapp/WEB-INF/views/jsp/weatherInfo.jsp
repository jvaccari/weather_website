<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Weather Info</title>

<spring:url value="/resources/core/css/bootstrap.min.css"
	var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
</head>

<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Weather Information</a>
		</div>
	</div>
</nav>

<div class="jumbotron">
	<div class="container">
		<h1>Weather info</h1>
		<p>

		</p>

		<form:form  commandname="command"  method="post">
			
			<fmt:setTimeZone value="${command.GMT}" />

			<table>
				<tr>
					<td><form:label path="currentDate">Current date</form:label></td>
					<fmt:formatDate value="${command.currentDate}" var="currentDateFormated" pattern="dd/MM/yyyy" />
					<td>${currentDateFormated}</td>
				</tr>
				<tr>
					<td><form:label path="cityName">City</form:label></td>
					<td>${command.cityName}</td>
				</tr>
				<tr>
					<td><form:label path="weatherDescription">Weather description</form:label></td>
					<td>${command.weatherDescription}</td>
				</tr>
				<tr>
					<td><form:label path="temperatureInCelsius">Temperature in Celsius</form:label></td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${command.temperatureInCelsius}" /></td>
				</tr>
				<tr>
					<td><form:label path="temperatureInFahrenheit">Temperature in Fahrenheit</form:label></td>
					<td><fmt:formatNumber type="number" maxFractionDigits="2" value="${command.temperatureInFahrenheit}" /></td>
				</tr>
				<tr>
					<td><form:label path="sunriseTime">Sunrise time</form:label></td>
					<fmt:formatDate value="${command.sunriseTime}" var="sunriseTimeFormatted" pattern="hh:mm a" />
					<td>${sunriseTimeFormatted}</td>
				</tr>
				<tr>
					<td><form:label path="sunsetTime">Sunset time</form:label></td>
					<fmt:formatDate value="${command.sunsetTime}" var="sunsetTimeFormatted" pattern="hh:mm a" />
					<td>${sunsetTimeFormatted}</td>
				</tr>
				
			</table>

		</form:form>


	</div>
</div>

<div class="container"></div>

<spring:url value="/resources/core/css/bootstrap.min.js"
	var="bootstrapJs" />

<script src="${bootstrapJs}"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</body>
</html>