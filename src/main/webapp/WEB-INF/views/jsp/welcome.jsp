<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Weather Info</title>

<spring:url value="/resources/core/css/bootstrap.min.css"
	var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
</head>

<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Weather Information</a>
		</div>
	</div>
</nav>

<div class="jumbotron">
	<div class="container">
		<h1>${title}</h1>
		<p>
			<c:if test="${empty name}">
				Welcome!
			</c:if>
		</p>

		<form:form action="getWeatherInfo" commandname="command" method="post">

			Choose city:
			<table>
				<tbody>
					<tr>
						<td>
							<ul>
								<form:select path="cityId" items="${cities}">
								</form:select>
							</ul>
						</td>
					</tr>

					<tr>
						<td colspan="2"><input type="submit" value="Submit" /></td>
					</tr>

				</tbody>
			</table>






		</form:form>


	</div>
</div>

<div class="container"></div>

<spring:url value="/resources/core/css/bootstrap.min.js"
	var="bootstrapJs" />

<script src="${bootstrapJs}"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</body>
</html>