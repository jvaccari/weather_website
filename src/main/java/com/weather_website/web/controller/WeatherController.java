package com.weather_website.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.weather_website.service.CountryInformationService;
import com.weather_website.service.WeatherService;
import com.weather_website.web.model.WeatherInfo;

@Controller
public class WeatherController {

	@Autowired
	private WeatherService weatherService;
	
	@Autowired
	private CountryInformationService countryInformationService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView weatherInfo() {
		 ModelAndView mav = new ModelAndView("welcome");  
		 mav.addObject("cities", countryInformationService.getCountriesByCountryIdMap());
		 mav.addObject("command", new WeatherInfo());
		 return mav;
	}

	@RequestMapping(value = "/getWeatherInfo", method = RequestMethod.POST)
	public String getWeatherInfo(@ModelAttribute("command") WeatherInfo weatherInfo, ModelMap model) {
		weatherInfo = weatherService.getWeatherInfo(new Long(weatherInfo.getCityId()));
		model.addAttribute("command", weatherInfo);
		return "weatherInfo";
	}
}