package com.weather_website.web.model.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherResponse {

	private Long id;
	private String name;
	private List<WeatherInstance> weather;
	private Main main;
	private Sys sys;
	
	public Main getMain() {
		return main;
	}
	public void setMain(Main main) {
		this.main = main;
	}
	public Sys getSys() {
		return sys;
	}
	public void setSys(Sys sys) {
		this.sys = sys;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<WeatherInstance> getWeather() {
		return weather;
	}
	public void setWeather(List<WeatherInstance> weather) {
		this.weather = weather;
	}

}
