package com.weather_website.web.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import com.weather_website.dao.TemperatureHelper;

public class WeatherInfo {

	private Long cityId;
	private String cityName;
	private Date currentDate;
	private String weatherDescription;
	private BigDecimal temperatureInCelsius;
	private BigDecimal temperatureInFahrenheit;
	private Date sunriseTime;
	private Date sunsetTime;
	private TimeZone timezone;
	
	public WeatherInfo() {
	}
	
	public WeatherInfo(Long cityId, String cityName, Date currentDate, String weatherDescription, BigDecimal temperatureInKelvin, Date sunriseTime, Date sunsetTime, TimeZone timezone) {
		this.cityId = cityId;
		this.cityName = cityName;
		this.currentDate = currentDate;
		this.weatherDescription = weatherDescription;
		this.temperatureInCelsius = TemperatureHelper.convertKelvinToCelsius(temperatureInKelvin);
		this.temperatureInFahrenheit = TemperatureHelper.convertKelvinToFahrenheit(temperatureInKelvin);
		this.sunriseTime = sunriseTime;
		this.sunsetTime = sunsetTime;
		this.timezone = timezone;
	}
	
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	public String getWeatherDescription() {
		return weatherDescription;
	}
	public void setWeatherDescription(String weatherDescription) {
		this.weatherDescription = weatherDescription;
	}
	public BigDecimal getTemperatureInCelsius() {
		return temperatureInCelsius;
	}
	public void setTemperatureInCelsius(BigDecimal temperatureInCelsius) {
		this.temperatureInCelsius = temperatureInCelsius;
	}
	public BigDecimal getTemperatureInFahrenheit() {
		return temperatureInFahrenheit;
	}
	public void setTemperatureInFahrenheit(BigDecimal temperatureInFahrenheit) {
		this.temperatureInFahrenheit = temperatureInFahrenheit;
	}
	public Date getSunriseTime() {
		return sunriseTime;
	}
	public void setSunriseTime(Date sunriseTime) {
		this.sunriseTime = sunriseTime;
	}
	public Date getSunsetTime() {
		return sunsetTime;
	}
	public void setSunsetTime(Date sunsetTime) {
		this.sunsetTime = sunsetTime;
	}

	public TimeZone getTimezone() {
		return timezone;
	}
		
	public String getGMT() {
		Long offset = TimeUnit.MILLISECONDS.toHours(timezone.getRawOffset());
		if (offset>0) {
			return "GMT+" + offset;
		}
		else {
			return "GMT" + offset;
		}
		
	}

	public void setTimezone(TimeZone timezone) {
		this.timezone = timezone;
	}
}
