package com.weather_website.dao;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import com.weather_website.config.EnvironmentConfiguration;
import com.weather_website.web.model.WeatherInfo;

/**
 * Cache for WeatherDaoOpenWeatherMapImpl since the weather does not change much and the API requests not to make too many calls
 * @author jvaccari
 */

@Repository
@Qualifier("daoCache")
public class WeatherDaoCacheImpl implements WeatherDao {

	@Autowired
	EnvironmentConfiguration envConf;
	
	private Map<Long, CacheWrapper> weatherInfoLocalStore = new ConcurrentHashMap<>();
	
	@Autowired
	private WeatherDao weatherDao;
	
	@Override
	public WeatherInfo getWeatherInfo(Long cityId) {
		if (cityId == null) {
			throw new IllegalArgumentException();
		}
		int secondsToCache = envConf.getSecondsToCache();
		WeatherInfo result;
		CacheWrapper cacheWrapper = weatherInfoLocalStore.get(cityId);
		if (cacheWrapper == null || cacheWrapper.getLastModified().before(new Date(System.currentTimeMillis() - 1000 * secondsToCache))) {
			result = weatherDao.getWeatherInfo(cityId);
			cacheWrapper = new CacheWrapper(new Date(), result);
			weatherInfoLocalStore.put(cityId, cacheWrapper);
		}
		else {
			result = cacheWrapper.getWeatherInfo();
		}
		return result;
	}

	public void setEnvConf(EnvironmentConfiguration envConf) {
		this.envConf = envConf;
	}

	public void setWeatherInfoLocalStore(Map<Long, CacheWrapper> weatherInfoLocalStore) {
		this.weatherInfoLocalStore = weatherInfoLocalStore;
	}

	public void setWeatherDao(WeatherDao weatherDao) {
		this.weatherDao = weatherDao;
	}
	
}
