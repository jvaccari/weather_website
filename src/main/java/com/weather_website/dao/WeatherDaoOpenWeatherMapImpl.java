package com.weather_website.dao;

import java.net.URISyntaxException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.weather_website.exception.WebServiceException;
import com.weather_website.service.CountryInformationService;
import com.weather_website.web.model.WeatherInfo;
import com.weather_website.web.model.response.WeatherResponse;

@Repository
public class WeatherDaoOpenWeatherMapImpl implements WeatherDao {

	private static final int MILLISECONDS_IN_A_SECOND = 1000;

	@Autowired
	private WeatherMapUrlHelper weatherMapHelper;
	
	@Autowired
	private CountryInformationService countryInformationService;
	
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public WeatherInfo getWeatherInfo(Long cityId) {
		if (cityId == null) {
			throw new IllegalArgumentException();
		}
		WeatherResponse weatherResponse = null;
		WeatherInfo weatherInfo = null;
		try {
			weatherResponse = restTemplate.getForObject(weatherMapHelper.generateUrlForCity(cityId), WeatherResponse.class);
			weatherInfo = generateWeatherInfo(weatherResponse);
		} catch (RestClientException | URISyntaxException e) {
			throw new WebServiceException();
		}
		return weatherInfo;
	}

	private WeatherInfo generateWeatherInfo(WeatherResponse weatherResponse) {
		return new WeatherInfo(weatherResponse.getId(),
				weatherResponse.getName(),
				new Date(),
				weatherResponse.getWeather().get(0).getDescription(),
				weatherResponse.getMain().getTemp(),
				new Date(weatherResponse.getSys().getSunrise() * MILLISECONDS_IN_A_SECOND),
				new Date(weatherResponse.getSys().getSunset() * MILLISECONDS_IN_A_SECOND),
				countryInformationService.getTimeZoneByCountryCode(weatherResponse.getSys().getCountry()));
	}

	public void setWeatherMapHelper(WeatherMapUrlHelper weatherMapHelper) {
		this.weatherMapHelper = weatherMapHelper;
	}

	public void setCountryInformationService(CountryInformationService countryInformationService) {
		this.countryInformationService = countryInformationService;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

}
