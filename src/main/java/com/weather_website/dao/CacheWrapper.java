package com.weather_website.dao;

import java.util.Date;

import com.weather_website.web.model.WeatherInfo;

public class CacheWrapper {

	private final Date lastModified;
	private final WeatherInfo weatherInfo;

	public CacheWrapper(Date lastModified, WeatherInfo weatherInfo) {
		this.lastModified = lastModified;
		this.weatherInfo = weatherInfo;
	}
	
	public Date getLastModified() {
		return lastModified;
	}

	public WeatherInfo getWeatherInfo() {
		return weatherInfo;
	}

	
}
