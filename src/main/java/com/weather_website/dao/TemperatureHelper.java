package com.weather_website.dao;

import java.math.BigDecimal;

public class TemperatureHelper {

	private static final BigDecimal fahrenheitConversionConstant = new BigDecimal("9").divide(new BigDecimal("5"));

	public static BigDecimal convertKelvinToCelsius(BigDecimal kelvinTemperature) {
		if (kelvinTemperature == null) {
			throw new IllegalArgumentException();
		}
		return kelvinTemperature.subtract(new BigDecimal("273.15"));
	}
	
	public static BigDecimal convertKelvinToFahrenheit(BigDecimal kelvinTemperature) {
		if (kelvinTemperature == null) {
			throw new IllegalArgumentException();
		}
		return (fahrenheitConversionConstant.multiply(kelvinTemperature)).subtract(new BigDecimal("459.67"));
	}

}
