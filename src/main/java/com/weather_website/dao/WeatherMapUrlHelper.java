package com.weather_website.dao;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.weather_website.config.EnvironmentConfiguration;

@Component
public class WeatherMapUrlHelper {

	@Autowired
	EnvironmentConfiguration envConf;
	
	public URI generateUrlForCity(Long cityId) throws URISyntaxException {
		if (cityId == null) throw new IllegalArgumentException();
		String key = envConf.getKey();
		String url = envConf.getUrl();
		String cityCall = url + "?id=" + cityId + "&APPID=" + key;
		return new URI(cityCall);
	}

	public void setEnvConf(EnvironmentConfiguration envConf) {
		this.envConf = envConf;
	}

}
