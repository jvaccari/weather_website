package com.weather_website.dao;

import com.weather_website.web.model.WeatherInfo;

public interface WeatherDao {
	WeatherInfo getWeatherInfo(Long cityId);
}
