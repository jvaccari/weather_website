package com.weather_website.config;

import javax.annotation.Resource;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:config.properties")
public class EnvironmentConfiguration {
	
	@Resource
	public Environment env;
	private String url;
	private String key;
	private int secondsToCache = 0;
	
	public String getUrl() {
		if (url==null) {
			url = env.getProperty("weather.service.url");
		}
		return url;
	}
	
	public String getKey() {
		if (key==null) {
			key = env.getProperty("weather.service.key");
		}
		return key;
	}

	public int getSecondsToCache() {
		if (secondsToCache == 0) {
			secondsToCache = new Integer(env.getProperty("weather.service.cacheTimeInSeconds"));
		}
		return secondsToCache;
	}

}
