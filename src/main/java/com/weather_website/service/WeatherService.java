package com.weather_website.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.weather_website.dao.WeatherDao;
import com.weather_website.web.model.WeatherInfo;

@Component
public class WeatherService {

	@Autowired
	@Qualifier("daoCache")
	WeatherDao weatherDao;

	public WeatherInfo getWeatherInfo(Long cityId) {
		return weatherDao.getWeatherInfo(cityId);
	}

}
