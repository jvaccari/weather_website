package com.weather_website.service;

import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

@Component
public class CountryInformationService {

	private final Map<String,TimeZone> timeZoneByCountryCode = new HashMap<>();
	private final Map<Long,String> countriesByCountryId = new HashMap<>();
	
	public CountryInformationService() {
		timeZoneByCountryCode.put("GB", TimeZone.getTimeZone("GB"));
		timeZoneByCountryCode.put("HK", TimeZone.getTimeZone("Asia/Hong_Kong"));
		
		countriesByCountryId.put(2643743l, "London");
		countriesByCountryId.put(1819729l, "Hong Kong");
	}

	public TimeZone getTimeZoneByCountryCode(String countryCode) {
		return timeZoneByCountryCode.get(countryCode);
	}

	public Map<Long, String> getCountriesByCountryIdMap() {
		return countriesByCountryId;
	}
	

}
