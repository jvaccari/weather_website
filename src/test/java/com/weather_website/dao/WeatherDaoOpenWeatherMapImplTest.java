package com.weather_website.dao;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.weather_website.exception.WebServiceException;
import com.weather_website.service.CountryInformationService;
import com.weather_website.web.model.WeatherInfo;
import com.weather_website.web.model.response.Main;
import com.weather_website.web.model.response.Sys;
import com.weather_website.web.model.response.WeatherInstance;
import com.weather_website.web.model.response.WeatherResponse;

public class WeatherDaoOpenWeatherMapImplTest {

	private WeatherMapUrlHelper weatherMapHelper;
	
	private CountryInformationService countryInformationService;
	
	private RestTemplate restTemplate;

	private WeatherDaoOpenWeatherMapImpl dao;
	
	@Before
	public void setup() {
		dao = new WeatherDaoOpenWeatherMapImpl();
		weatherMapHelper = Mockito.mock(WeatherMapUrlHelper.class);
		countryInformationService = Mockito.mock(CountryInformationService.class);
		restTemplate = Mockito.mock(RestTemplate.class);
		dao.setWeatherMapHelper(weatherMapHelper);
		dao.setRestTemplate(restTemplate);
		dao.setCountryInformationService(countryInformationService);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void exceptionThrownWhenIllegalArgument() {
		dao.getWeatherInfo(null);
	}
	
	@Test
	public void testNormalCallToWebServiceWithoutAnyProblems() throws URISyntaxException {
		
		URI uri = new URI("http://api.openweathermap.org/data/2.5/weather?id=1&APPID=da27657cf493b53ea2266fd8a55faaaa");
		WeatherResponse weatherResponse = createWeatherResponse();
		
		when(weatherMapHelper.generateUrlForCity(1l)).thenReturn(uri);
		when(restTemplate.getForObject(uri,WeatherResponse.class)).thenReturn(weatherResponse);
		
		WeatherInfo weatherInfo = dao.getWeatherInfo(1l);
		
		assertEquals("sunny", weatherInfo.getWeatherDescription());
	}
	
	@Test(expected=WebServiceException.class)
	public void testErrorInWebServiceThrowsException() throws URISyntaxException {
		URI uri = new URI("http://api.openweathermap.org/data/2.5/weather?id=1&APPID=da27657cf493b53ea2266fd8a55faaaa");
		
		when(weatherMapHelper.generateUrlForCity(1l)).thenReturn(uri);
		when(restTemplate.getForObject(uri,WeatherResponse.class)).thenThrow(new RestClientException("error"));
		
		dao.getWeatherInfo(1l);
	}
	
	private WeatherResponse createWeatherResponse() {
		
		WeatherResponse weatherResponse = new WeatherResponse();
		
		WeatherInstance weatherInstance = new WeatherInstance();
		weatherInstance.setDescription("sunny");
		List<WeatherInstance> weatherInstances = new ArrayList<>();
		weatherInstances.add(weatherInstance);
		weatherResponse.setWeather(weatherInstances);
		
		Main main = new Main();
		main.setTemp(new BigDecimal("100"));
		weatherResponse.setMain(main);
		
		Sys sys = new Sys();
		sys.setCountry("GB");
		sys.setSunrise(1475474784l);
		sys.setSunset(1475515859l);
		weatherResponse.setSys(sys);
		
		return weatherResponse;
		
	}
	
}
