package com.weather_website.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.weather_website.config.EnvironmentConfiguration;
import com.weather_website.web.model.WeatherInfo;

public class WeatherDaoCacheImplTest {

	private WeatherDaoCacheImpl dao;
	
	private WeatherDao innerDao = Mockito.mock(WeatherDao.class);;
	
	private EnvironmentConfiguration envConf = Mockito.mock(EnvironmentConfiguration.class);;
	
	@Before
	public void setup() {
		dao = new WeatherDaoCacheImpl();
		dao.setEnvConf(envConf);
		dao.setWeatherDao(innerDao);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void exceptionThrownWhenIllegalArgument() {
		dao.getWeatherInfo(null);
	}
	
	@Test
	public void notExistentWeatherInfoRequestShouldAskInnerDao() {
		WeatherInfo weatherInfo = new WeatherInfo();
		weatherInfo.setCityId(1l);
		
		when(envConf.getSecondsToCache()).thenReturn(120);
		when(innerDao.getWeatherInfo(1l)).thenReturn(weatherInfo);
		
		WeatherInfo returnedWeatherInfo = dao.getWeatherInfo(1l);
		
		verify(envConf, times(1)).getSecondsToCache();
		verify(innerDao, times(1)).getWeatherInfo(1l);
		
		assertEquals(weatherInfo, returnedWeatherInfo);
	}
	
	@Test
	public void staleCacheInformationShouldBeReplacedWithInnerDaoInfo() {
		WeatherInfo weatherInfo = new WeatherInfo();
		weatherInfo.setCityId(1l);
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -5);
		Date anOldDate = cal.getTime();
		
		CacheWrapper wrapper = new CacheWrapper(anOldDate, weatherInfo);
		
		Map<Long, CacheWrapper> weatherInfoLocalStore = new ConcurrentHashMap<>();
		weatherInfoLocalStore.put(1l, wrapper);
		
		dao.setWeatherInfoLocalStore(weatherInfoLocalStore);
		
		when(envConf.getSecondsToCache()).thenReturn(120);
		when(innerDao.getWeatherInfo(1l)).thenReturn(weatherInfo);
		
		WeatherInfo returnedWeatherInfo = dao.getWeatherInfo(1l);
		
		verify(envConf, times(1)).getSecondsToCache();
		verify(innerDao, times(1)).getWeatherInfo(1l);
		
		CacheWrapper newWrapper = weatherInfoLocalStore.get(1l);

		assertTrue(newWrapper.getLastModified().after(anOldDate));
		assertEquals(weatherInfo, returnedWeatherInfo);
		
	}

	@Test
	public void upToDateInfoShouldBeRetrievedFromCache() {
		WeatherInfo weatherInfo = new WeatherInfo();
		weatherInfo.setCityId(1l);
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -2);
		Date aDate = cal.getTime();
		
		CacheWrapper wrapper = new CacheWrapper(aDate, weatherInfo);
		
		Map<Long, CacheWrapper> weatherInfoLocalStore = new ConcurrentHashMap<>();
		weatherInfoLocalStore.put(1l, wrapper);
		
		dao.setWeatherInfoLocalStore(weatherInfoLocalStore);
		
		when(envConf.getSecondsToCache()).thenReturn(240);
		
		WeatherInfo returnedWeatherInfo = dao.getWeatherInfo(1l);
		
		verify(envConf, times(1)).getSecondsToCache();
		verify(innerDao, times(0)).getWeatherInfo(1l);
		
		CacheWrapper newWrapper = weatherInfoLocalStore.get(1l);

		assertTrue(newWrapper.getLastModified().equals(aDate));
		assertEquals(weatherInfo, returnedWeatherInfo);

	}
}
