package com.weather_website.dao;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

public class TemperatureHelperTest {

	@Test
	public void testCelsiusConversion() {
		BigDecimal resultInCelsius = TemperatureHelper.convertKelvinToCelsius(new BigDecimal("10"));
		assertEquals(new BigDecimal("-263.15"), resultInCelsius);
	}
	
	@Test
	public void testFahrenheitConversion() {
		BigDecimal resultInCelsius = TemperatureHelper.convertKelvinToFahrenheit(new BigDecimal("10"));
		assertEquals(new BigDecimal("-441.67"), resultInCelsius);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testCelsiusConversionNullParameter() {
		TemperatureHelper.convertKelvinToCelsius(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFahrenheitConversionNullParameter() {
		TemperatureHelper.convertKelvinToFahrenheit(null);
	}
	
}
