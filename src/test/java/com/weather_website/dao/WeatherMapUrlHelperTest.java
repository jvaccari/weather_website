package com.weather_website.dao;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.weather_website.config.EnvironmentConfiguration;

public class WeatherMapUrlHelperTest {
	
	private WeatherMapUrlHelper helper;
	
	@Before
	public void setup() {
		this.helper = new WeatherMapUrlHelper();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void nullParameterThrowsException() throws URISyntaxException {
		EnvironmentConfiguration envConf = Mockito.mock(EnvironmentConfiguration.class);
		when(envConf.getKey()).thenReturn("somekey");
		when(envConf.getUrl()).thenReturn("http://api.openweathermap.org/data/2.5/weather");
		helper.setEnvConf(envConf);
		helper.generateUrlForCity(null);
	}

	@Test(expected=URISyntaxException.class)
	public void wrongFilePropertiesThrowsException() throws URISyntaxException {
		EnvironmentConfiguration envConf = Mockito.mock(EnvironmentConfiguration.class);
		when(envConf.getKey()).thenReturn("somekey");
		when(envConf.getUrl()).thenReturn("!£$£%%@%@");
		helper.setEnvConf(envConf);
		helper.generateUrlForCity(1l);
	}
	
	public void generatesURIForId() throws URISyntaxException {
		EnvironmentConfiguration envConf = Mockito.mock(EnvironmentConfiguration.class);
		when(envConf.getKey()).thenReturn("somekey");
		when(envConf.getUrl()).thenReturn("http://api.openweathermap.org/data/2.5/weather");
		helper.setEnvConf(envConf);
		URI uri = helper.generateUrlForCity(5l);
		assertEquals("dfdfdsf", uri.toString());
	}
	
	
}
